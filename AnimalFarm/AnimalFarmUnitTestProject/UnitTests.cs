﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AnimalFarm;
using System.Collections.Generic;

namespace AnimalFarmUnitTest
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void HerdCreationSuccessful()
        {
            int youngAmount = 15;
            int matureAmount = 30;
            int oldAmount = 10;
            double matureReproduceCoefficient = 0.15;
            double oldReproduceCoefficient = 0.05;
            double youngSurvivalCoefficient = 0.9;
            double oldDyingCoefficient = 0.1;
            List<object> expectedParameters = new List<object>() { youngAmount, matureAmount, oldAmount, matureReproduceCoefficient, oldReproduceCoefficient, youngSurvivalCoefficient, oldDyingCoefficient };
            Herd herd = new Herd(youngAmount, matureAmount, oldAmount, matureReproduceCoefficient, oldReproduceCoefficient, youngSurvivalCoefficient, oldDyingCoefficient);
            List<object> herdParameters = new List<object>() { herd.YoungAmount, herd.MatureAmount, herd.OldAmount, herd.MatureReproduceCoefficient,
                                                                herd.OldReproduceCoefficient, herd.YoungSurvivalCoefficient, herd.OldDyingCoefficient };
            CollectionAssert.AreEqual(expectedParameters, herdParameters);
        }

        [TestMethod]
        public void GettingHerdTotalAmountSuccessful()
        {
            int youngAmount = 15;
            int matureAmount = 30;
            int oldAmount = 10;
            double matureReproduceCoefficient = 0.15;
            double oldReproduceCoefficient = 0.05;
            double youngSurvivalCoefficient = 0.9;
            double oldDyingCoefficient = 0.1;
            int expectedTotal = youngAmount + matureAmount + oldAmount;
            Herd herd = new Herd(youngAmount, matureAmount, oldAmount, matureReproduceCoefficient, oldReproduceCoefficient, youngSurvivalCoefficient, oldDyingCoefficient);
            Assert.AreEqual(expectedTotal, herd.TotalAmount);
        }

        [TestMethod]
        public void ChangingHerdTotalAmountCorrect()
        {
            int youngAmount = 15;
            int matureAmount = 30;
            int oldAmount = 10;
            double matureReproduceCoefficient = 0.15;
            double oldReproduceCoefficient = 0.05;
            double youngSurvivalCoefficient = 0.9;
            double oldDyingCoefficient = 0.1;
            int expectedTotal = youngAmount + matureAmount + oldAmount;
            int addAmount = 11;
            int subtractAmount = 7;
            Herd herd = new Herd(youngAmount, matureAmount, oldAmount, matureReproduceCoefficient, oldReproduceCoefficient, youngSurvivalCoefficient, oldDyingCoefficient);

            herd.YoungAmount += addAmount;
            expectedTotal += addAmount;
            herd.MatureAmount += addAmount;
            expectedTotal += addAmount;
            herd.OldAmount += addAmount;
            expectedTotal += addAmount;
            Assert.AreEqual(expectedTotal, herd.TotalAmount);

            herd.YoungAmount -= subtractAmount;
            expectedTotal -= subtractAmount;
            herd.MatureAmount -= subtractAmount;
            expectedTotal -= subtractAmount;
            herd.OldAmount -= subtractAmount;
            expectedTotal -= subtractAmount;
            Assert.AreEqual(expectedTotal, herd.TotalAmount);
        }

        [TestMethod]
        public void AnnualContractCreationSuccessful()
        {
            int foodAmount = 100;
            int foodPrice = 250;
            int foodPenalty = 500;
            int youngAmount = 20;
            int youngPrice = 800;
            int youngPenalty = 1600;
            int matureAmount = 65;
            int maturePrice = 1000;
            int maturePenalty = 1000;
            int oldAmount = 15;
            int oldPrice = 600;
            int oldPenalty = 1200;
            AnnualContract annualContract = new AnnualContract(foodAmount, foodPrice, foodPenalty, youngAmount, youngPrice,
                                                                youngPenalty, matureAmount, maturePrice, maturePenalty, oldAmount, oldPrice, oldPenalty);
            List<object> expectedParameters = new List<object>() { foodAmount, foodPrice, foodPenalty, youngAmount, youngPrice,
                                                                youngPenalty, matureAmount, maturePrice, maturePenalty, oldAmount, oldPrice, oldPenalty };
            List<object> annualContractParameters = new List<object>() { annualContract.FoodAmount, annualContract.FoodPrice, annualContract.FoodPenalty,
                                                                         annualContract.YoungAmount, annualContract.YoungPrice, annualContract.YoungPenalty,
                                                                         annualContract.MatureAmount, annualContract.MaturePrice, annualContract.MaturePenalty,
                                                                         annualContract.OldAmount, annualContract.OldPrice, annualContract.OldPenalty };
            CollectionAssert.AreEqual(expectedParameters, annualContractParameters);
        }

        [TestMethod]
        public void DoLifeCycleEnoughFoodCorrect()
        {
            int youngAmount = 15;
            int matureAmount = 30;
            int oldAmount = 10;
            double matureReproduceCoefficient = 0.15;
            double oldReproduceCoefficient = 0.05;
            double youngSurvivalCoefficient = 0.9;
            double oldDyingCoefficient = 0.1;
            Herd herd = new Herd(youngAmount, matureAmount, oldAmount, matureReproduceCoefficient, oldReproduceCoefficient, youngSurvivalCoefficient, oldDyingCoefficient);
            int foodAmount = (youngAmount + matureAmount + oldAmount) * 100;
            IHerdProcessor herdProcessor = new HerdProcessor();
            herdProcessor.DoLifeCycle(herd, foodAmount);
            int expectedYoungAmount = Convert.ToInt32(matureAmount * matureReproduceCoefficient + oldAmount * oldReproduceCoefficient);
            int expectedMatureAmount = Convert.ToInt32(youngAmount * youngSurvivalCoefficient);
            int expectedOldAmount = Convert.ToInt32(oldAmount * (1 - oldDyingCoefficient) + matureAmount);
            Assert.AreEqual(expectedYoungAmount, herd.YoungAmount);
            Assert.AreEqual(expectedMatureAmount, herd.MatureAmount);
            Assert.AreEqual(expectedOldAmount, herd.OldAmount);
        }

        [TestMethod]
        public void DoLifeCycleNotEnoughFoodCorrect()
        {
            int youngAmount = 200;
            int matureAmount = 500;
            int oldAmount = 300;
            double matureReproduceCoefficient = 0.15;
            double oldReproduceCoefficient = 0.05;
            double youngSurvivalCoefficient = 0.9;
            double oldDyingCoefficient = 0.1;
            Herd herd = new Herd(youngAmount, matureAmount, oldAmount, matureReproduceCoefficient, oldReproduceCoefficient, youngSurvivalCoefficient, oldDyingCoefficient);
            int foodAmount = (youngAmount / 2 + matureAmount + oldAmount / 3) / 2;
            IHerdProcessor herdProcessor = new HerdProcessor();
            herdProcessor.DoLifeCycle(herd, foodAmount);
            int expectedYoungAmount = Convert.ToInt32(matureAmount / 2 * matureReproduceCoefficient + oldAmount / 2 * oldReproduceCoefficient);
            int expectedMatureAmount = Convert.ToInt32(youngAmount * youngSurvivalCoefficient) / 2;
            int expectedOldAmount = Convert.ToInt32(oldAmount * (1 - oldDyingCoefficient) + matureAmount) / 2;
            Assert.AreEqual(expectedYoungAmount, herd.YoungAmount);
            Assert.AreEqual(expectedMatureAmount, herd.MatureAmount);
            Assert.AreEqual(expectedOldAmount, herd.OldAmount);
        }

        [TestMethod]
        public void BuyFoodEnoughMoneyCorrect()
        {
            int balance = 50000;
            int foodAmount = 100;
            int foodPrice = 20;
            int expectedBalance = balance - foodAmount * foodPrice;
            AnnualContract contract = new AnnualContract(foodAmount, foodPrice);
            IContractProcessor contractProcessor = new ContractProcessor();
            ContractProcessorInfo foodInfo = contractProcessor.BuyFood(contract, ref balance);
            Assert.AreEqual(foodAmount, foodInfo.Amount);
            Assert.AreEqual(expectedBalance, balance);
        }

        [TestMethod]
        public void BuyFoodNotEnoughMoneyCorrect()
        {
            int balance = 1000;
            int foodAmount = 100;
            int foodPrice = 20;
            int foodPenalty = 10;
            int expectedBalance = balance - (balance / foodPrice) * foodPrice - (foodAmount - balance / foodPrice) * foodPenalty;
            AnnualContract contract = new AnnualContract(foodAmount, foodPrice, foodPenalty, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            IContractProcessor contractProcessor = new ContractProcessor();
            ContractProcessorInfo foodInfo = contractProcessor.BuyFood(contract, ref balance);
            Assert.AreNotEqual(foodAmount, foodInfo.Amount);
            Assert.AreEqual(expectedBalance, balance);
        }

        [TestMethod]
        public void SellAnimalsWithoutPenaltyCorrect()
        {
            int balance = 0;
            int youngAmount = 10;
            int matureAmount = 15;
            int oldAmount = 20;
            int youngToSell = 4;
            int youngPrice = 100;
            int matureToSell = 7;
            int maturePrice = 120;
            int oldToSell = 11;
            int oldPrice = 80;

            int expectedBalance = balance + youngToSell * youngPrice + matureToSell * maturePrice + oldToSell * oldPrice;
            int expectedYoung = youngAmount - youngToSell;
            int expectedMature = matureAmount - matureToSell;
            int expectedOld = oldAmount - oldToSell;

            Herd herd = new Herd(youngAmount, matureAmount, oldAmount);
            IHerdProcessor herdProcessor = new HerdProcessor();
            AnnualContract contract = new AnnualContract(0, 0, 0, youngToSell, youngPrice, 0, matureToSell, maturePrice, 0, oldToSell, oldPrice);
            IContractProcessor contractProcessor = new ContractProcessor();
            contractProcessor.SellAnimals(contract, herd, ref balance);
            
            Assert.AreEqual(expectedBalance, balance);
            Assert.AreEqual(expectedYoung, herd.YoungAmount);
            Assert.AreEqual(expectedMature, herd.MatureAmount);
            Assert.AreEqual(expectedOld, herd.OldAmount);
        }

        [TestMethod]
        public void SellAnimalsWithPenaltyCorrect()
        {
            int balance = 0;
            int youngAmount = 10;
            int matureAmount = 15;
            int oldAmount = 20;
            int youngToSell = 11;
            int youngPrice = 100;
            int matureToSell = 16;
            int maturePrice = 120;
            int oldToSell = 21;
            int oldPrice = 80;
            int youngPenalty = 150;
            int maturePenalty = 200;
            int oldPenalty = 120;

            int expectedBalance = balance + youngAmount * youngPrice - ((youngToSell - youngAmount) * youngPenalty) + matureAmount * maturePrice - ((matureToSell - matureAmount) * maturePenalty) + oldAmount * oldPrice - ((oldToSell - oldAmount) * oldPenalty);
            int expectedYoung = 0;
            int expectedMature = 0;
            int expectedOld = 0;

            Herd herd = new Herd(youngAmount, matureAmount, oldAmount);
            IHerdProcessor herdProcessor = new HerdProcessor();
            AnnualContract contract = new AnnualContract(0, 0, 0, youngToSell, youngPrice, youngPenalty, matureToSell, maturePrice, maturePenalty, oldToSell, oldPrice, oldPenalty);
            IContractProcessor contractProcessor = new ContractProcessor();
            contractProcessor.SellAnimals(contract, herd, ref balance);
            
            Assert.AreEqual(expectedBalance, balance);
            Assert.AreEqual(expectedYoung, herd.YoungAmount);
            Assert.AreEqual(expectedMature, herd.MatureAmount);
            Assert.AreEqual(expectedOld, herd.OldAmount);
        }
    }
}
