﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace AnimalFarm
{
    /// <summary>
    /// Логика взаимодействия для SetupWindow.xaml
    /// </summary>
    public partial class SetupWindow : Window
    {
        private List<AnnualContract> contracts;
        private List<Tuple<TextBox, Button, Button>> controlsList;
        private bool isInitialized = false;
        private bool coefficientsCorrect = true;

        public SetupWindow()
        {
            InitializeComponent();
            isInitialized = true;
            controlsList = new List<Tuple<TextBox, Button, Button>>()
            {
                new Tuple<TextBox, Button, Button>(TxtBalance, BtnPlus, BtnMinus),
                new Tuple<TextBox, Button, Button>(TxtYoung, BtnPlusYoung, BtnMinusYoung),
                new Tuple<TextBox, Button, Button>(TxtMature, BtnPlusMature, BtnMinusMature),
                new Tuple<TextBox, Button, Button>(TxtOld, BtnPlusOld, BtnMinusOld)
            };
            foreach (Tuple<TextBox, Button, Button> triple in controlsList)
            {
                TxtParameter_TextChanged(triple.Item1, null);
            }
            TxtYoung.TextChanged += TxtAnimals_TextChanged;
            TxtMature.TextChanged += TxtAnimals_TextChanged;
            TxtOld.TextChanged += TxtAnimals_TextChanged;
            contracts = ModellingWindow.annualContracts;
            lblInputErrorMessage.Content = "";
            UpdateContractList();
            CheckAllFields();
            
        }

        private void btnAddContract_Click(object sender, RoutedEventArgs e)
        {
            AnnualContract contract = new AnnualContract();
            if (contracts.Count > 0)
            {
                contract = (AnnualContract)contracts[contracts.Count - 1].Clone();
            }
            ContractWindow ctrWindow = new ContractWindow(contract);
            ctrWindow.ShowDialog();
            contracts.Add(contract);
            UpdateContractList();
        }

        private void btnEditContract_Click(object sender, RoutedEventArgs e)
        {
            if (listContracts.SelectedItem != null)
            {
                ContractWindow ctrWindow = new ContractWindow(listContracts.SelectedItem as AnnualContract);
                ctrWindow.ShowDialog();
            }
            UpdateContractList();
        }

        private void btnRemoveContract_Click(object sender, RoutedEventArgs e)
        {
            if (listContracts.SelectedItem != null)
            {
                contracts.Remove(listContracts.SelectedItem as AnnualContract);
                listContracts.Items.Remove(listContracts.SelectedItem);
            }
            UpdateContractList();
        }

        private void UpdateContractList()
        {
            listContracts.Items.Clear();
            foreach (AnnualContract contract in contracts)
            {
                listContracts.Items.Add(contract);
            }
            CheckAllFields();
        }
#region Buttons
        private void BtnPlus_Click(object sender, RoutedEventArgs e)
        {
            TxtBalance.Text = Convert.ToString(int.Parse(TxtBalance.Text) + 100);
        }

        private void BtnMinus_Click(object sender, RoutedEventArgs e)
        {
            TxtBalance.Text = Convert.ToString(int.Parse(TxtBalance.Text) - 100);
            if(int.Parse(TxtBalance.Text) <= 0)
                TxtBalance.Text = Convert.ToString(0);
        }

        private void BtnPlusYoung_Click(object sender, RoutedEventArgs e)
        {
            TxtYoung.Text = Convert.ToString(int.Parse(TxtYoung.Text) + 1);
        }

        private void BtnMinusYoung_Click(object sender, RoutedEventArgs e)
        {
            TxtYoung.Text = Convert.ToString(int.Parse(TxtYoung.Text) - 1);
            if (int.Parse(TxtYoung.Text) <= 0)
                TxtYoung.Text = Convert.ToString(0);
        }

        private void BtnPlusMature_Click(object sender, RoutedEventArgs e)
        {
            TxtMature.Text = Convert.ToString(int.Parse(TxtMature.Text) + 1);
        }

        private void BtnMinusMature_Click(object sender, RoutedEventArgs e)
        {
            TxtMature.Text = Convert.ToString(int.Parse(TxtMature.Text) - 1);
            if (int.Parse(TxtMature.Text) <= 0)
                TxtMature.Text = Convert.ToString(0);
        }

        private void BtnPlusOld_Click(object sender, RoutedEventArgs e)
        {
            TxtOld.Text = Convert.ToString(int.Parse(TxtOld.Text) + 1);
        }

        private void BtnMinusOld_Click(object sender, RoutedEventArgs e)
        {
            TxtOld.Text = Convert.ToString(int.Parse(TxtOld.Text) - 1);
            if (int.Parse(TxtOld.Text) <= 0)
                TxtOld.Text = Convert.ToString(0);
        }
        #endregion

        private void btnFinishCreating_Click(object sender, RoutedEventArgs e)
        {
            ModellingWindow.balance = int.Parse(TxtBalance.Text);
            ModellingWindow.herd.YoungAmount = int.Parse(TxtYoung.Text);
            ModellingWindow.herd.MatureAmount = int.Parse(TxtMature.Text);
            ModellingWindow.herd.OldAmount = int.Parse(TxtOld.Text);
            ModellingWindow.herd.YoungSurvivalCoefficient = Convert.ToDouble(int.Parse(TxtYSC.Text))/100;
            ModellingWindow.herd.MatureReproduceCoefficient = double.Parse(TxtMRC.Text);
            ModellingWindow.herd.OldReproduceCoefficient = double.Parse(TxtORC.Text);
            ModellingWindow.herd.OldDyingCoefficient = Convert.ToDouble(int.Parse(TxtODC.Text))/100;
            ModellingWindow.annualContracts = contracts;
            ModellingWindow.EventChance = Convert.ToInt32(slider.Value);
            ModellingWindow.setupClosedCorrectly = true;
            Close();
        }

        private void TxtAnimals_TextChanged(object sender, TextChangedEventArgs e)
        {
            int valueYoung, valueMature, valueOld;
            if (ContractWindow.IsDigit(TxtYoung.Text) && ContractWindow.IsDigit(TxtMature.Text) &&
                ContractWindow.IsDigit(TxtOld.Text) && int.TryParse(TxtYoung.Text, out valueYoung) &&
                int.TryParse(TxtMature.Text, out valueMature) && int.TryParse(TxtOld.Text, out valueOld))
                LblTotalAmount.Content = valueYoung + valueMature + valueOld;
            else
                LblTotalAmount.Content = 0;
        }

        private void slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            LblEventCurrent.Content = Convert.ToInt32(slider.Value);
        }

        private void TxtParameter_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (isInitialized)
            {
                if (ContractWindow.IsDigit((sender as TextBox).Text))
                {
                    if ((sender as TextBox).Text.Length > 8)
                        (sender as TextBox).Text = "0";
                    foreach (Tuple<TextBox, Button, Button> triple in controlsList)
                    {
                        if (sender.Equals(triple.Item1))
                        {
                            triple.Item2.IsEnabled = true;
                            int temp;
                            if (int.TryParse((sender as TextBox).Text, out temp) && (temp > 0))
                                triple.Item3.IsEnabled = true;
                            else
                                triple.Item3.IsEnabled = false;
                            if (!int.TryParse((sender as TextBox).Text, out temp))
                            {
                                triple.Item2.IsEnabled = false;
                                triple.Item3.IsEnabled = false;
                            }
                        }
                    }
                }
                else if ((sender as TextBox).Text.StartsWith("-") &&
                         ContractWindow.IsDigit((sender as TextBox).Text.Substring(1, (sender as TextBox).Text.Length - 1)))
                {
                    if ((sender as TextBox).Text.Length > 9)
                        (sender as TextBox).Text = "0";
                    foreach (Tuple<TextBox, Button, Button> triple in controlsList)
                    {
                        if (sender.Equals(triple.Item1))
                        {
                            triple.Item2.IsEnabled = true;
                            triple.Item3.IsEnabled = false;
                        }
                    }
                }
                else
                {
                    foreach (Tuple<TextBox, Button, Button> triple in controlsList)
                    {
                        if (sender.Equals(triple.Item1))
                        {
                            triple.Item2.IsEnabled = false;
                            triple.Item3.IsEnabled = false;
                        }
                    }

                }
                CheckAllFields();
            }
        }

        private void CheckCoefficients(object sender, TextChangedEventArgs e)
        {
            if (isInitialized)
            {
                int tmp;
                double temp;
                if (double.TryParse(TxtMRC.Text, out temp) && double.TryParse(TxtORC.Text, out temp) &&
                    ContractWindow.IsDigit(TxtYSC.Text) && int.TryParse(TxtYSC.Text, out tmp) && ContractWindow.IsDigit(TxtODC.Text) && int.TryParse(TxtODC.Text, out tmp))
                {
                    coefficientsCorrect = true;
                }
                else
                {
                    coefficientsCorrect = false;
                }
            }
            CheckAllFields();
        }

        private bool CheckContracts()
        {
            if (isInitialized)
            {
                if (contracts != null && (contracts.Count < 3 || contracts.Count > 5))
                {
                    return false;
                }
                else
                    return true;
            }
            return false;
        }

        private bool CheckAllFields()
        {
            bool result = false;
            if (isInitialized)
            {
                int tmp;
                bool allFieldsAreCorrect = CheckFields();
                if (allFieldsAreCorrect && ContractWindow.IsDigit(TxtBalance.Text) &&
                    int.TryParse(TxtBalance.Text, out tmp) &&
                    int.TryParse(Convert.ToString(LblTotalAmount.Content), out tmp) && (tmp >= 0) && coefficientsCorrect &&
                    CheckContracts())
                {
                    lblInputErrorMessage.Content = "";
                    btnFinishCreating.IsEnabled = true;
                    result = true;
                }
                else
                {
                    if (allFieldsAreCorrect && ContractWindow.IsDigit(TxtBalance.Text) &&
                        int.TryParse(TxtBalance.Text, out tmp) &&
                        int.TryParse(Convert.ToString(LblTotalAmount.Content), out tmp) && (tmp >= 0) &&
                        coefficientsCorrect && !CheckContracts())
                        lblInputErrorMessage.Content = "Необходимо от 3 до 5 контрактов";
                    else
                        lblInputErrorMessage.Content = "Некоторые поля заполнены некорректно или пусты.";
                    btnFinishCreating.IsEnabled = false;
                    result = false;
                }
            }
            return result;
        }

        private bool CheckFields()
        {
            bool allFieldsAreCorrect = true;
            foreach (var textBox in GridHerd.Children)
            {
                int temp;
                if ((textBox is TextBox) && (!ContractWindow.IsDigit((textBox as TextBox).Text) || !int.TryParse((textBox as TextBox).Text, out temp)))
                {
                    allFieldsAreCorrect = false;
                    break;
                }
            }
            return allFieldsAreCorrect;
        }
    }
}
