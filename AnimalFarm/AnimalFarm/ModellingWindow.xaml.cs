﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace AnimalFarm
{ 
    public partial class ModellingWindow : Window
    {
        public static int balance = 0;
        public static int balanceBeforeModeling = 0;
        public static Herd herd = new Herd();
        public static Herd herdBeforeModeling = new Herd();
        public static List<AnnualContract> annualContracts = new List<AnnualContract>();
        public static bool setupClosedCorrectly = false;
        public static string events = String.Empty;
        static Random random = new Random();
        public static int EventChance = 0;
        static int adverseEventHerdDeathMinPercent = 5;
        static int adverseEventHerdDeathMaxPercent = 20;

        public static int currentContractNumber = -1;
        IHerdProcessor herdProcessor = new HerdProcessor();
        IContractProcessor contractProcessor = new ContractProcessor();


        public ModellingWindow()
        {
            InitializeComponent();
            SetupWindow setupWindow = new SetupWindow();
            setupWindow.ShowDialog();
            herdBeforeModeling = (Herd)herd.Clone();
            balanceBeforeModeling = balance;
            if (setupClosedCorrectly == false)
                Close();
            else
                RefreshAll();
        }

        private void RefreshAll()
        {
            // Параметры фермы
            lblBalance.Content = ReduceNumber(balance);
            lblFarmYoungAmount.Content = ReduceNumber(herd.YoungAmount);
            lblFarmMatureAmount.Content = ReduceNumber(herd.MatureAmount);
            lblFarmOldAmount.Content = ReduceNumber(herd.OldAmount);
            lblFarmTotalAmount.Content = ReduceNumber(herd.TotalAmount);
            lblEventChance.Content = EventChance;
            if (currentContractNumber == -1) // Начало моделирование
            {
                lblAnimalBalance.Content = ReduceNumber(CountAnimalBalance(herd, annualContracts[currentContractNumber + 1]));
                lblGeneralBalance.Content = ReduceNumber(balance + CountAnimalBalance(herd, annualContracts[currentContractNumber + 1]));
            }
            else
            {
                lblAnimalBalance.Content = ReduceNumber(CountAnimalBalance(herd, annualContracts[currentContractNumber]));
                lblGeneralBalance.Content = ReduceNumber(balance + CountAnimalBalance(herd, annualContracts[currentContractNumber]));
            }
            lblBirthCoefMature.Content = herd.MatureReproduceCoefficient;
            lblBirthCoefOld.Content = herd.OldReproduceCoefficient;
            lblYoungSurvive.Content = Convert.ToInt32(herd.YoungSurvivalCoefficient * 100);
            lblOldDying.Content = Convert.ToInt32(herd.OldDyingCoefficient * 100);

            // Параметры текущего контракта
            if (currentContractNumber == -1)
            {
                lblFoodAmount.Content = "none";
                lblFoodPrice.Content = "none";
                lblFoodPenalty.Content = "none";
                lblYoungAmount.Content = "none";
                lblYoungPrice.Content = "none";
                lblYoungPenalty.Content = "none";
                lblMatureAmount.Content = "none";
                lblMaturePrice.Content = "none";
                lblMaturePenalty.Content = "none";
                lblOldAmount.Content = "none";
                lblOldPrice.Content = "none";
                lblOldPenalty.Content = "none";
            }
            else
            {
                lblFoodAmount.Content = ReduceNumber(annualContracts[currentContractNumber].FoodAmount);
                lblFoodPrice.Content = ReduceNumber(annualContracts[currentContractNumber].FoodPrice);
                lblFoodPenalty.Content = ReduceNumber(annualContracts[currentContractNumber].FoodPenalty);
                lblYoungAmount.Content = ReduceNumber(annualContracts[currentContractNumber].YoungAmount);
                lblYoungPrice.Content = ReduceNumber(annualContracts[currentContractNumber].YoungPrice);
                lblYoungPenalty.Content = ReduceNumber(annualContracts[currentContractNumber].YoungPenalty);
                lblMatureAmount.Content = ReduceNumber(annualContracts[currentContractNumber].MatureAmount);
                lblMaturePrice.Content = ReduceNumber(annualContracts[currentContractNumber].MaturePrice);
                lblMaturePenalty.Content = ReduceNumber(annualContracts[currentContractNumber].MaturePenalty);
                lblOldAmount.Content = ReduceNumber(annualContracts[currentContractNumber].OldAmount);
                lblOldPrice.Content = ReduceNumber(annualContracts[currentContractNumber].OldPrice);
                lblOldPenalty.Content = ReduceNumber(annualContracts[currentContractNumber].OldPenalty);
            }

            // Параметры следующего контракта
            
            if (currentContractNumber + 1 < annualContracts.Count)
            {
                lblFoodAmountN.Content = ReduceNumber(annualContracts[currentContractNumber + 1].FoodAmount);
                lblFoodPriceN.Content = ReduceNumber(annualContracts[currentContractNumber + 1].FoodPrice);
                lblFoodPenaltyN.Content = ReduceNumber(annualContracts[currentContractNumber + 1].FoodPenalty);
                lblYoungAmountN.Content = ReduceNumber(annualContracts[currentContractNumber + 1].YoungAmount);
                lblYoungPriceN.Content = ReduceNumber(annualContracts[currentContractNumber + 1].YoungPrice);
                lblYoungPenaltyN.Content = ReduceNumber(annualContracts[currentContractNumber + 1].YoungPenalty);
                lblMatureAmountN.Content = ReduceNumber(annualContracts[currentContractNumber + 1].MatureAmount);
                lblMaturePriceN.Content = ReduceNumber(annualContracts[currentContractNumber + 1].MaturePrice);
                lblMaturePenaltyN.Content = ReduceNumber(annualContracts[currentContractNumber + 1].MaturePenalty);
                lblOldAmountN.Content = ReduceNumber(annualContracts[currentContractNumber + 1].OldAmount);
                lblOldPriceN.Content = ReduceNumber(annualContracts[currentContractNumber + 1].OldPrice);
                lblOldPenaltyN.Content = ReduceNumber(annualContracts[currentContractNumber + 1].OldPenalty);
            }
            else
            {
                lblFoodAmountN.Content = "none";
                lblFoodPriceN.Content = "none";
                lblFoodPenaltyN.Content = "none";
                lblYoungAmountN.Content = "none";
                lblYoungPriceN.Content = "none";
                lblYoungPenaltyN.Content = "none";
                lblMatureAmountN.Content = "none";
                lblMaturePriceN.Content = "none";
                lblMaturePenaltyN.Content = "none";
                lblOldAmountN.Content = "none";
                lblOldPriceN.Content = "none";
                lblOldPenaltyN.Content = "none";
            }

            // Контрактов осталось
            lblContractsRemain.Content = annualContracts.Count - (currentContractNumber + 1);

            // Смена надписи на кнопке
            if (currentContractNumber + 1 >= annualContracts.Count())
                btnNextYear.Content = "Вывести результаты";

        }

        public static int CountAnimalBalance (Herd herd, AnnualContract currentContract)
        {
            int balance = 0;

            balance += herd.YoungAmount * currentContract.YoungPrice;
            balance += herd.MatureAmount * currentContract.MaturePrice;
            balance += herd.OldAmount * currentContract.OldPrice;

            return balance;
        }

        private void btnNextYear_Click(object sender, RoutedEventArgs e)
        {
            if (currentContractNumber + 1 < annualContracts.Count())
            {                
                currentContractNumber++;
                SpendYear();
                RefreshAll();
            }
            else
            {
                events = txbEvents.Text;
                FinalWindow finalWindow = new FinalWindow();
                finalWindow.ShowDialog();
                Close();
            }
        }

        private void SpendYear()
        {
            WriteEventMessage("");
            WriteEventMessage(String.Format("Выполнение контракта №{0}:", currentContractNumber + 1));
            WriteEventMessage("");
            ContractProcessorInfo foodInfo = contractProcessor.BuyFood(annualContracts[currentContractNumber], ref balance);
            string foodMessage = String.Format("Закуплено {0} единиц корма по цене {1} за единицу", foodInfo.Amount, foodInfo.Price);
            if (foodInfo.PenaltyWasPaid)
                foodMessage += String.Format(". За недозакупленный корм была была выплачена неустойка в размере {0}", foodInfo.Penalty);
            WriteEventMessage(foodMessage);

            Herd previousHerdState = (Herd)herd.Clone();

            int starvingDiePercent = herdProcessor.DoLifeCycle(herd, foodInfo.Amount);
            WriteEventMessage(String.Format("В течение года популяция молодых особей изменилась с {0} до {1}, взрослых - с {2} до {3}, старых - с {4} до {5}. От голода погибло {6}% животных. Общая численность - c {7} до {8}"
                                                , previousHerdState.YoungAmount, herd.YoungAmount, previousHerdState.MatureAmount, herd.MatureAmount, previousHerdState.OldAmount, herd.OldAmount, starvingDiePercent, previousHerdState.TotalAmount, herd.TotalAmount));

            bool adverseEventHappened = CheckAdverseEvent(); // Неблагоприятное событие

            List<ContractProcessorInfo> sellInfo = contractProcessor.SellAnimals(annualContracts[currentContractNumber], herd, ref balance);
            string youngMessage = String.Format("Продано {0} молодых особей по цене {1} за особь", sellInfo[0].Amount, sellInfo[0].Price);
            if (sellInfo[0].PenaltyWasPaid)
                youngMessage += String.Format(". За недопроданных молодых особей была выплачена неустойка в размере {0}", sellInfo[0].Penalty);
            WriteEventMessage(youngMessage);

            string matureMessage = String.Format("Продано {0} взрослых особей по цене {1} за особь", sellInfo[1].Amount, sellInfo[1].Price);
            if (sellInfo[1].PenaltyWasPaid)
                matureMessage += String.Format(". За недопроданных взрослых особей была выплачена неустойка в размере {0}", sellInfo[1].Penalty);
            WriteEventMessage(matureMessage);

            string oldMessage = String.Format("Продано {0} старых особей по цене {1} за особь", sellInfo[2].Amount, sellInfo[2].Price);
            if (sellInfo[0].PenaltyWasPaid)
                oldMessage += String.Format(". За недопроданных старых особей была выплачена неустойка в размере {0}", sellInfo[2].Penalty);
            WriteEventMessage(oldMessage);
            WriteEventMessage(String.Format("Численность стада: {0} молодых, {1} взрослых, {2} старых. Всего: {3} особей.", herd.YoungAmount, herd.MatureAmount, herd.OldAmount, herd.TotalAmount));
            bool farmIsBankrupt = CheckBankruptcy();
            if (farmIsBankrupt)
                currentContractNumber = annualContracts.Count() - 1;
        }

        private void WriteEventMessage(string message)
        {
            txbEvents.Text += Environment.NewLine;
            txbEvents.Text += message;
            txbEvents.ScrollToEnd();
        }

        private bool CheckBankruptcy()
        {
            if (balance < 0)
            {
                MessageBox.Show(String.Format("Баланс фермы: {0}.", balance) + Environment.NewLine + "Ферма обанкрочена. Моделирование остановлено.");
                return true;
            }
            else
                return false;
        }

        private bool CheckAdverseEvent()
        {
            if (random.Next(101) <= EventChance)
            {
                int herdDeathPercent = random.Next(adverseEventHerdDeathMinPercent, adverseEventHerdDeathMaxPercent + 1);
                herdProcessor.ChangePopulationProportionally(herd, Convert.ToDouble(100 - herdDeathPercent) / 100);
                WriteEventMessage(String.Format("Внимание! Вследствие неблагоприятных условий погибло {0}% особей. Численность стада составляет: {1} молодых особей, {2} - взрослых, {3} - старых. Общая численность: {4} особей", herdDeathPercent, herd.YoungAmount, herd.MatureAmount, herd.OldAmount, herd.TotalAmount));
                return true;
            }
            return false;
        }

        private string ReduceNumber(int number)
        {
            string result = number.ToString();
            if (number.ToString().Length >= 6)
            {
                result = number.ToString().Substring(0, number.ToString().Length - 3) + "k";
            }
            return result;
        }
    }
}
