﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace AnimalFarm
{
    /// <summary>
    /// Логика взаимодействия для ContractWindow.xaml
    /// </summary>
    public partial class ContractWindow : Window
    {
        private AnnualContract newContract;
        private bool isInitialized = false;
        private List<Tuple<TextBox, Button, Button>> controlsList;
        public ContractWindow(AnnualContract contract)
        {
            InitializeComponent();
            isInitialized = true;
            controlsList = new List<Tuple<TextBox, Button, Button>>()
            {
                new Tuple<TextBox, Button, Button>(TxtFoodAmount, BtnFoodAmountPlus, BtnFoodAmountMinus),
                new Tuple<TextBox, Button, Button>(TxtFoodPrice, BtnFoodPricePlus, BtnFoodPriceMinus),
                new Tuple<TextBox, Button, Button>(TxtFoodPenalty, BtnFoodPenaltyPlus, BtnFoodPenaltyMinus),
                new Tuple<TextBox, Button, Button>(TxtYoungAmount, btnYoungAmountPlus, btnYoungAmountMinus),
                new Tuple<TextBox, Button, Button>(TxtYoungPrice, btnYoungPricePlus, btnYoungPriceMinus),
                new Tuple<TextBox, Button, Button>(TxtYoungPenalty, btnYoungPenaltyPlus, btnYoungPenaltyMinus),
                new Tuple<TextBox, Button, Button>(TxtMatureAmount, btnMatureAmountPlus, btnMatureAmountMinus),
                new Tuple<TextBox, Button, Button>(TxtMaturePrice, btnMaturePricePlus, btnMaturePriceMinus),
                new Tuple<TextBox, Button, Button>(TxtMaturePenalty, btnMaturePenaltyPlus, btnMaturePenaltyMinus),
                new Tuple<TextBox, Button, Button>(TxtOldAmount, btnOldAmountPlus, btnOldAmountMinus),
                new Tuple<TextBox, Button, Button>(TxtOldPrice, btnOldPricePlus, btnOldPriceMinus),
                new Tuple<TextBox, Button, Button>(TxtOldPenalty, btnOldPenaltyPlus, btnOldPenaltyMinus)
            };
            foreach (Tuple<TextBox, Button, Button> triple in controlsList)
            {
                TxtParameter_TextChanged(triple.Item1, null);
            }
            newContract = contract;
            TxtFoodAmount.Text = Convert.ToString(newContract.FoodAmount);
            TxtFoodPrice.Text = Convert.ToString(newContract.FoodPrice);
            TxtFoodPenalty.Text = Convert.ToString(newContract.FoodPenalty);
            TxtYoungAmount.Text = Convert.ToString(newContract.YoungAmount);
            TxtYoungPrice.Text = Convert.ToString(newContract.YoungPrice);
            TxtYoungPenalty.Text = Convert.ToString(newContract.YoungPenalty);
            TxtMatureAmount.Text = Convert.ToString(newContract.MatureAmount);
            TxtMaturePrice.Text = Convert.ToString(newContract.MaturePrice);
            TxtMaturePenalty.Text = Convert.ToString(newContract.MaturePenalty);
            TxtOldAmount.Text = Convert.ToString(newContract.OldAmount);
            TxtOldPrice.Text = Convert.ToString(newContract.OldPrice);
            TxtOldPenalty.Text = Convert.ToString(newContract.OldPenalty);
        }

        private void btnAddContract_Click(object sender, RoutedEventArgs e)
        {
            newContract.FoodAmount = int.Parse(TxtFoodAmount.Text);
            newContract.FoodPrice = int.Parse(TxtFoodPrice.Text);
            newContract.FoodPenalty = int.Parse(TxtFoodPenalty.Text);
            newContract.YoungAmount = int.Parse(TxtYoungAmount.Text);
            newContract.YoungPrice = int.Parse(TxtYoungPrice.Text);
            newContract.YoungPenalty = int.Parse(TxtYoungPenalty.Text);
            newContract.MatureAmount = int.Parse(TxtMatureAmount.Text);
            newContract.MaturePrice = int.Parse(TxtMaturePrice.Text);
            newContract.MaturePenalty = int.Parse(TxtMaturePenalty.Text);
            newContract.OldAmount = int.Parse(TxtOldAmount.Text);
            newContract.OldPrice = int.Parse(TxtOldPrice.Text);
            newContract.OldPenalty = int.Parse(TxtOldPenalty.Text);
            Close();
        }
#region FoodParameters
        private void BtnFoodAmountPlus_Click(object sender, RoutedEventArgs e)
        {
            TxtFoodAmount.Text = Convert.ToString(int.Parse(TxtFoodAmount.Text) + 10);
        }

        private void BtnFoodAmountMinus_Click(object sender, RoutedEventArgs e)
        {
            TxtFoodAmount.Text = Convert.ToString(int.Parse(TxtFoodAmount.Text) - 10);
            if (int.Parse(TxtFoodAmount.Text) <= 0)
                TxtFoodAmount.Text = Convert.ToString(0);
        }

        private void BtnFoodPricePlus_Click(object sender, RoutedEventArgs e)
        {
            TxtFoodPrice.Text = Convert.ToString(int.Parse(TxtFoodPrice.Text) + 10);
        }

        private void BtnFoodPriceMinus_Click(object sender, RoutedEventArgs e)
        {
            TxtFoodPrice.Text = Convert.ToString(int.Parse(TxtFoodPrice.Text) - 10);
            if (int.Parse(TxtFoodPrice.Text) <= 0)
                TxtFoodPrice.Text = Convert.ToString(0);
        }

        private void BtnFoodPenaltyPlus_Click(object sender, RoutedEventArgs e)
        {
            TxtFoodPenalty.Text = Convert.ToString(int.Parse(TxtFoodPenalty.Text) + 10);
        }

        private void BtnFoodPenaltyMinus_Click(object sender, RoutedEventArgs e)
        {
            TxtFoodPenalty.Text = Convert.ToString(int.Parse(TxtFoodPenalty.Text) - 10);
            if (int.Parse(TxtFoodPenalty.Text) <= 0)
                TxtFoodPenalty.Text = Convert.ToString(0);
        }
        #endregion
#region YoungAnimals
        private void btnYoungAmountPlus_Click(object sender, RoutedEventArgs e)
        {
            TxtYoungAmount.Text = Convert.ToString(int.Parse(TxtYoungAmount.Text) + 1);
        }

        private void btnYoungAmountMinus_Click(object sender, RoutedEventArgs e)
        {
            TxtYoungAmount.Text = Convert.ToString(int.Parse(TxtYoungAmount.Text) - 1);
            if (int.Parse(TxtYoungAmount.Text) <= 0)
                TxtYoungAmount.Text = Convert.ToString(0);
        }

        private void btnYoungPricePlus_Click(object sender, RoutedEventArgs e)
        {
            TxtYoungPrice.Text = Convert.ToString(int.Parse(TxtYoungPrice.Text) + 10);
        }

        private void btnYoungPriceMinus_Click(object sender, RoutedEventArgs e)
        {
            TxtYoungPrice.Text = Convert.ToString(int.Parse(TxtYoungPrice.Text) - 10);
            if (int.Parse(TxtYoungPrice.Text) <= 0)
                TxtYoungPrice.Text = Convert.ToString(0);
        }

        private void btnYoungPenaltyPlus_Click(object sender, RoutedEventArgs e)
        {
            TxtYoungPenalty.Text = Convert.ToString(int.Parse(TxtYoungPenalty.Text) + 10);
        }

        private void btnYoungPenaltyMinus_Click(object sender, RoutedEventArgs e)
        {
            TxtYoungPenalty.Text = Convert.ToString(int.Parse(TxtYoungPenalty.Text) - 10);
            if (int.Parse(TxtYoungPenalty.Text) <= 0)
                TxtYoungPenalty.Text = Convert.ToString(0);
        }
        #endregion
#region MatureAnimals
        private void btnMatureAmountPlus_Click(object sender, RoutedEventArgs e)
        {
            TxtMatureAmount.Text = Convert.ToString(int.Parse(TxtMatureAmount.Text) + 1);
        }

        private void btnMatureAmountMinus_Click(object sender, RoutedEventArgs e)
        {
            TxtMatureAmount.Text = Convert.ToString(int.Parse(TxtMatureAmount.Text) - 1);
            if (int.Parse(TxtMatureAmount.Text) <= 0)
                TxtMatureAmount.Text = Convert.ToString(0);
        }

        private void btnMaturePricePlus_Click(object sender, RoutedEventArgs e)
        {
            TxtMaturePrice.Text = Convert.ToString(int.Parse(TxtMaturePrice.Text) + 10);
        }

        private void btnMaturePriceMinus_Click(object sender, RoutedEventArgs e)
        {
            TxtMaturePrice.Text = Convert.ToString(int.Parse(TxtMaturePrice.Text) - 10);
            if (int.Parse(TxtMaturePrice.Text) <= 0)
                TxtMaturePrice.Text = Convert.ToString(0);
        }

        private void btnMaturePenaltyPlus_Click(object sender, RoutedEventArgs e)
        {
            TxtMaturePenalty.Text = Convert.ToString(int.Parse(TxtMaturePenalty.Text) + 10);
        }

        private void btnMaturePenaltyMinus_Click(object sender, RoutedEventArgs e)
        {
            TxtMaturePenalty.Text = Convert.ToString(int.Parse(TxtMaturePenalty.Text) - 10);
            if (int.Parse(TxtMaturePenalty.Text) <= 0)
                TxtMaturePenalty.Text = Convert.ToString(0);
        }
        #endregion
#region OldAnimals
        private void btnOldAmountPlus_Click(object sender, RoutedEventArgs e)
        {
            TxtOldAmount.Text = Convert.ToString(int.Parse(TxtOldAmount.Text) + 1);
        }

        private void btnOldAmountMinus_Click(object sender, RoutedEventArgs e)
        {
            TxtOldAmount.Text = Convert.ToString(int.Parse(TxtOldAmount.Text) - 1);
            if (int.Parse(TxtOldAmount.Text) <= 0)
                TxtOldAmount.Text = Convert.ToString(0);
        }

        private void btnOldPricePlus_Click(object sender, RoutedEventArgs e)
        {
            TxtOldPrice.Text = Convert.ToString(int.Parse(TxtOldPrice.Text) + 10);
        }

        private void btnOldPriceMinus_Click(object sender, RoutedEventArgs e)
        {
            TxtOldPrice.Text = Convert.ToString(int.Parse(TxtOldPrice.Text) - 10);
            if (int.Parse(TxtOldPrice.Text) <= 0)
                TxtOldPrice.Text = Convert.ToString(0);
        }

        private void btnOldPenaltyPlus_Click(object sender, RoutedEventArgs e)
        {
            TxtOldPenalty.Text = Convert.ToString(int.Parse(TxtOldPenalty.Text) + 10);
        }

        private void btnOldPenaltyMinus_Click(object sender, RoutedEventArgs e)
        {
            TxtOldPenalty.Text = Convert.ToString(int.Parse(TxtOldPenalty.Text) - 10);
            if (int.Parse(TxtOldPenalty.Text) <= 0)
                TxtOldPenalty.Text = Convert.ToString(0);
        }
        #endregion

        private void TxtParameter_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (isInitialized)
            {
                if (IsDigit((sender as TextBox).Text))
                {
                    foreach (Tuple<TextBox, Button, Button> triple in controlsList)
                    {
                        if (sender.Equals(triple.Item1))
                        {
                            triple.Item2.IsEnabled = true;
                            int temp;
                            if (int.TryParse((sender as TextBox).Text, out temp) && (temp > 0))
                                triple.Item3.IsEnabled = true;
                            else
                                triple.Item3.IsEnabled = false;
                            if (!int.TryParse((sender as TextBox).Text, out temp))
                            {
                                triple.Item2.IsEnabled = false;
                                triple.Item3.IsEnabled = false;
                            }
                        }
                    }
                }
                else if ((sender as TextBox).Text.StartsWith("-") &&
                         IsDigit((sender as TextBox).Text.Substring(1, (sender as TextBox).Text.Length - 1)))
                {
                   foreach (Tuple<TextBox, Button, Button> triple in controlsList)
                    {
                        if (sender.Equals(triple.Item1))
                        {
                            triple.Item2.IsEnabled = true;
                            triple.Item3.IsEnabled = false;
                        }
                    }
                }
                else
                {
                    foreach (Tuple<TextBox, Button, Button> triple in controlsList)
                    {
                        if (sender.Equals(triple.Item1))
                        {
                            triple.Item2.IsEnabled = false;
                            triple.Item3.IsEnabled = false;
                        }
                    }

                }
                bool allFieldsAreCorrect = true;
                foreach (var textBox in MainGrid.Children)
                {
                    int temp;
                    if ((textBox is TextBox) && (!IsDigit((textBox as TextBox).Text) || !int.TryParse((textBox as TextBox).Text, out temp)))
                    {
                        allFieldsAreCorrect = false;
                        break;
                    }
                }
                if (allFieldsAreCorrect)
                    btnAddContract.IsEnabled = true;
                else
                    btnAddContract.IsEnabled = false;
            }
        }

        public static bool IsDigit(string input)
        {
            if (input.Equals(String.Empty))
                return false;
            bool result = true;
            foreach (char c in input)
            {
                if (!Char.IsDigit(c))
                {
                    result = false;
                    break;
                }
            }
            return result;
        }
    }
}
