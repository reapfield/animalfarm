﻿using System;
using System.Windows;
using Microsoft.Win32;
using System.IO;

namespace AnimalFarm
{
    /// <summary>
    /// Логика взаимодействия для FinalWindow.xaml
    /// </summary>
    public partial class FinalWindow : Window
    {
        public FinalWindow()
        {
            InitializeComponent();
            ShowResults();
        }

        private void ShowResults()
        {
            AddMessage("Капитал до начала моделирования:");
            AddMessage("");
            AddMessage(String.Format("Денежный капитал: {0}", ModellingWindow.balanceBeforeModeling));
            AddMessage(String.Format("Стоимость стада по условиям первого контракта: {0}",
                ModellingWindow.CountAnimalBalance(ModellingWindow.herdBeforeModeling,
                    ModellingWindow.annualContracts[0])));
            AddMessage(String.Format("Общий капитал: {0}",
                ModellingWindow.balanceBeforeModeling +
                ModellingWindow.CountAnimalBalance(ModellingWindow.herdBeforeModeling,
                    ModellingWindow.annualContracts[0])));
            AddMessage("");
            AddMessage("Параметры фермы до начала моделирования:");
            AddMessage("");
            AddMessage(String.Format("Количество молодых особей: {0}", ModellingWindow.herdBeforeModeling.YoungAmount));
            AddMessage(String.Format("Количество взрослых особей: {0}", ModellingWindow.herdBeforeModeling.MatureAmount));
            AddMessage(String.Format("Количество старых особей: {0}", ModellingWindow.herdBeforeModeling.OldAmount));
            AddMessage(String.Format("Всего особей: {0}", ModellingWindow.herdBeforeModeling.TotalAmount));
            AddMessage("");
            AddMessage("Биологические коэффициенты стада:");
            AddMessage("");
            AddMessage(String.Format("Коэффициент рождаемости у взрослых особей: {0} молодых особей на одну взрослую",
                ModellingWindow.herdBeforeModeling.MatureReproduceCoefficient));
            AddMessage(String.Format("Коэффициент рождаемости у старых особей: {0} молодых особей на одну старую",
                ModellingWindow.herdBeforeModeling.OldReproduceCoefficient));
            AddMessage(String.Format("Процент выживаемости молодых особей: {0}%",
                Convert.ToInt32(ModellingWindow.herdBeforeModeling.YoungSurvivalCoefficient*100)));
            AddMessage(String.Format("Процент смертности старых особей: {0}%",
                Convert.ToInt32(ModellingWindow.herdBeforeModeling.OldDyingCoefficient*100)));
            AddMessage("");
            AddMessage("Параметры ежегодных контрактов:");
            AddMessage("");
            int year = 1;
            foreach (AnnualContract contract in ModellingWindow.annualContracts)
            {
                AddMessage(String.Format("Параметры контракта №{0}:", year));
                AddMessage(String.Format("Покупка корма. Количество: {0}, цена: {1}, неустойка: {2}",
                    contract.FoodAmount, contract.FoodPrice, contract.FoodPenalty));
                AddMessage(String.Format("Продажа молодых особей. Количество: {0}, цена: {1}, неустойка: {2}",
                    contract.YoungAmount, contract.YoungPrice, contract.YoungPenalty));
                AddMessage(String.Format("Продажа взрослых особей. Количество: {0}, цена: {1}, неустойка: {2}",
                    contract.MatureAmount, contract.MaturePrice, contract.MaturePenalty));
                AddMessage(String.Format("Продажа старых особей. Количество: {0}, цена: {1}, неустойка: {2}",
                    contract.OldAmount, contract.OldPrice, contract.OldPenalty));
                year++;
            }
            AddMessage("");
            AddMessage("Параметры стада после моделирования:");
            AddMessage("");
            AddMessage(String.Format("Количество молодых особей: {0}", ModellingWindow.herd.YoungAmount));
            AddMessage(String.Format("Количество взрослых особей: {0}", ModellingWindow.herd.MatureAmount));
            AddMessage(String.Format("Количество старых особей: {0}", ModellingWindow.herd.OldAmount));
            AddMessage(String.Format("Всего особей: {0}", ModellingWindow.herd.TotalAmount));
            AddMessage("");
            AddMessage("Капитал после моделирования:");
            AddMessage("");
            AddMessage(String.Format("Денежный капитал: {0}", ModellingWindow.balance));
            AddMessage(String.Format("Стоимость стада по условиям последнего контракта: {0}",
                ModellingWindow.CountAnimalBalance(ModellingWindow.herd,
                    ModellingWindow.annualContracts[ModellingWindow.currentContractNumber])));
            AddMessage(String.Format("Общий капитал: {0}",
                ModellingWindow.balance +
                ModellingWindow.CountAnimalBalance(ModellingWindow.herd,
                    ModellingWindow.annualContracts[ModellingWindow.currentContractNumber])));
            AddMessage("");
            AddMessage("Лог событий:");
            AddMessage("");
            AddMessage(ModellingWindow.events);
            AddMessage("");
            AddMessage("Итог:");
            int generalCapitalBeforeModeling = ModellingWindow.balanceBeforeModeling +
                                               ModellingWindow.CountAnimalBalance(ModellingWindow.herdBeforeModeling,
                                                   ModellingWindow.annualContracts[0]);
            int generalCapitalAfterModeling = ModellingWindow.balance +
                                              ModellingWindow.CountAnimalBalance(ModellingWindow.herd,
                                                  ModellingWindow.annualContracts[ModellingWindow.currentContractNumber]);
            if (generalCapitalAfterModeling < generalCapitalBeforeModeling)
                AddMessage(String.Format("Данная модель неэффективна. Общий капитал уменьшился с {0} до {1}. Убыль: -{2} (-{3:0.00}%)", generalCapitalBeforeModeling, generalCapitalAfterModeling, generalCapitalBeforeModeling - generalCapitalAfterModeling, Convert.ToDouble(generalCapitalBeforeModeling - generalCapitalAfterModeling) / Convert.ToDouble(generalCapitalBeforeModeling) * 100));
            if (generalCapitalAfterModeling == generalCapitalBeforeModeling)
                AddMessage("Данная модель неэффективна. Общий капитал не изменился");
            if (generalCapitalAfterModeling > generalCapitalBeforeModeling)
                AddMessage(
                    String.Format(
                        "Данная модель эффективна. Общий капитал увеличился с {0} до {1}. Прибыль: {2} ({3:0.00}%)",
                        generalCapitalBeforeModeling, generalCapitalAfterModeling,
                        generalCapitalAfterModeling - generalCapitalBeforeModeling,
                        Convert.ToDouble(generalCapitalAfterModeling - generalCapitalBeforeModeling)/
                        Convert.ToDouble(generalCapitalBeforeModeling)*100));
        }

        private void AddMessage(string message)
        {
            txbResults.Text += message;
            txbResults.Text += Environment.NewLine;
        }

        private void btnSaveAndQuit_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog.Title = "Сохранить";
            saveFileDialog.ShowDialog();

            if (saveFileDialog.FileName != String.Empty)
            {
                try
                {
                    File.WriteAllText(saveFileDialog.FileName, txbResults.Text);
                }
                catch (IOException ex)
                {
                    MessageBox.Show(string.Format("При попытке записи в файл произошла ошибка: {0}", ex.Message));
                }
                Close();
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
