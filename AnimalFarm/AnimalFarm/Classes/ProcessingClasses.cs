﻿using System;
using System.Collections.Generic;

namespace AnimalFarm
{
    public interface IHerdProcessor
    {
        int DoLifeCycle(Herd herd, int foodAmount);
        void ChangePopulationProportionally(Herd herd, double changeCoefficient);
    }


    public class HerdProcessor: IHerdProcessor
    {
        public int DoLifeCycle(Herd herd, int foodAmount)
        {
            int starvingDiePercent = 0;
            starvingDiePercent = Feeding(herd, foodAmount);
            NaturallyChangePopulation(herd);
            return starvingDiePercent;
        }

        private int Feeding( Herd herd, int foodAmount)
        {
            int starvingDiePercent = 0;
            int necessaryFoodAmount = Convert.ToInt32((herd.YoungAmount / 2) + (herd.MatureAmount) + (herd.OldAmount / 3));
            double foodCoefficient = 1;
            if (necessaryFoodAmount > 0)
                foodCoefficient = Convert.ToDouble(foodAmount) / Convert.ToDouble(necessaryFoodAmount);
            if (foodCoefficient < 1)
            {
                ChangePopulationProportionally(herd, foodCoefficient);
                starvingDiePercent = 100 - Convert.ToInt32(foodCoefficient*100);
            }
            return starvingDiePercent;
        }

        public void ChangePopulationProportionally(Herd herd, double changeCoefficient)
        {            
            herd.YoungAmount = Convert.ToInt32(herd.YoungAmount * changeCoefficient);
            herd.MatureAmount = Convert.ToInt32(herd.MatureAmount * changeCoefficient);
            herd.OldAmount = Convert.ToInt32(herd.OldAmount * changeCoefficient);
        }

        private void NaturallyChangePopulation(Herd herd)
        {
            int newYoungAmount = Convert.ToInt32(herd.MatureReproduceCoefficient * herd.MatureAmount + herd.OldReproduceCoefficient * herd.OldAmount);
            int newMatureAmount = Convert.ToInt32(herd.YoungSurvivalCoefficient * herd.YoungAmount);
            int newOldAmount = Convert.ToInt32(herd.MatureAmount + (1 - herd.OldDyingCoefficient) * herd.OldAmount);
            herd.YoungAmount = newYoungAmount;
            herd.MatureAmount = newMatureAmount;
            herd.OldAmount = newOldAmount;
        }
    }    

    public interface IContractProcessor
    {
        ContractProcessorInfo BuyFood(AnnualContract contract, ref int balance);
        List<ContractProcessorInfo> SellAnimals(AnnualContract contract, Herd herd, ref int balance);
    }

    public class ContractProcessorInfo
    {
        public int Amount = 0;
        public int Price = 0;
        public bool PenaltyWasPaid = false;
        public int Penalty = 0;
    }

    public class ContractProcessor : IContractProcessor
    {
        public ContractProcessorInfo BuyFood(AnnualContract contract, ref int balance)
        {
            ContractProcessorInfo results = new ContractProcessorInfo();
            int foodPossibleToBuy = 0;
            if (contract.FoodPrice != 0)
                foodPossibleToBuy = balance / contract.FoodPrice;
            else
                foodPossibleToBuy = contract.FoodAmount;
            results.Price = contract.FoodPrice;
            if (foodPossibleToBuy >= contract.FoodAmount)
            {
                balance -= contract.FoodAmount*contract.FoodPrice;
                results.Amount = contract.FoodAmount;                
                results.PenaltyWasPaid = false;
            }
            else
            {
                balance -= foodPossibleToBuy*contract.FoodPrice;
                results.Amount = foodPossibleToBuy;
                int penalty = (contract.FoodAmount - foodPossibleToBuy) * contract.FoodPenalty;
                results.Penalty = penalty;
                balance -= penalty;
                results.PenaltyWasPaid = true;                
            }
            return results;
        }

        public List<ContractProcessorInfo> SellAnimals(AnnualContract contract, Herd herd, ref int balance)
        {
            List<ContractProcessorInfo> results = new List<ContractProcessorInfo>();

            Tuple<int, ContractProcessorInfo> youngResults = CountAnimalsAfterSelling(contract.YoungAmount, contract.YoungPrice, herd.YoungAmount, contract.YoungPenalty, ref balance);
            herd.YoungAmount = youngResults.Item1;
            results.Add(youngResults.Item2);
            Tuple<int, ContractProcessorInfo> matureResults = CountAnimalsAfterSelling(contract.MatureAmount, contract.MaturePrice, herd.MatureAmount, contract.MaturePenalty, ref balance);
            herd.MatureAmount = matureResults.Item1;
            results.Add(matureResults.Item2);
            Tuple<int, ContractProcessorInfo> oldResults = CountAnimalsAfterSelling(contract.OldAmount, contract.OldPrice, herd.OldAmount, contract.OldPenalty, ref balance);
            herd.OldAmount = oldResults.Item1;
            results.Add(oldResults.Item2);

            return results;
        }

        private Tuple<int, ContractProcessorInfo> CountAnimalsAfterSelling(int necessaryAmountToSell, int priceForEach, int possibleAmountToSell, int penaltyForEachUnsold, ref int balance)
        {
            ContractProcessorInfo results = new ContractProcessorInfo();
            results.Price = priceForEach;
            int newAnimalsAmount = 0;
            if (possibleAmountToSell >= necessaryAmountToSell)
            {
                newAnimalsAmount = possibleAmountToSell - necessaryAmountToSell;
                if ((long) (necessaryAmountToSell*priceForEach) > int.MaxValue)
                    balance = int.MaxValue;
                else
                    balance += necessaryAmountToSell * priceForEach;
                results.Amount = necessaryAmountToSell;
                results.PenaltyWasPaid = false;
            }
            else
            {
                newAnimalsAmount = 0;
                if ((long)(necessaryAmountToSell * priceForEach) > int.MaxValue)
                    balance = int.MaxValue;
                else
                    balance += necessaryAmountToSell * priceForEach;
                results.Amount = possibleAmountToSell;
                int penalty = (necessaryAmountToSell - possibleAmountToSell) * penaltyForEachUnsold;
                results.Penalty = penalty;
                balance -= penalty;
                results.PenaltyWasPaid = true;
            }
            return new Tuple<int, ContractProcessorInfo>(newAnimalsAmount, results);
        }
    }
}
