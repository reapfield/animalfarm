﻿using System;

namespace AnimalFarm
{
    public class Herd : ICloneable
    {
        private int _youngAmount;
        private int _matureAmount;
        private int _oldAmount;

#region Свойства
        public int YoungAmount
        {
            get
            {
                return _youngAmount;
            }
            set
            {
                if (value < 0)
                    _youngAmount = 0;
                else if (value > int.MaxValue)
                {
                    _youngAmount = int.MaxValue;
                }
                else
                {
                    _youngAmount = value;
                }
            }
        }

        public int MatureAmount
        {
            get
            {
                return _matureAmount;
            }
            set
            {
                if (value < 0)
                    _matureAmount = 0;
                else if (value > int.MaxValue)
                {
                    _matureAmount = int.MaxValue;
                }
                else
                {
                    _matureAmount = value;
                }
            }
        }

        public int OldAmount
        {
            get
            {
                return _oldAmount;
            }
            set
            {
                if (value < 0)
                    _oldAmount = 0;
                else if (value > int.MaxValue)
                {
                    _oldAmount = int.MaxValue;
                }
                else
                {
                    _oldAmount = value;
                }
            }
        }
#endregion

        public int TotalAmount
        {
            get
            {
                if ((long)(YoungAmount + MatureAmount + OldAmount) > int.MaxValue)
                    return int.MaxValue;
                else
                    return YoungAmount + MatureAmount + OldAmount;
            }
        }

        public double MatureReproduceCoefficient { get; set; } //alpha
        public double OldReproduceCoefficient { get; set; } //beta
        public double YoungSurvivalCoefficient { get; set; } //delta
        public double OldDyingCoefficient { get; set; } //rghoe       

        public Herd(int youngAmount = 0, int matureAmount = 0, int oldAmount = 0, double matureReproduceCoefficient = 0, double oldReproduceCoefficient = 0, double youngSurvivalCoefficient = 0, double oldDyingCoefficient = 0)
        {
            YoungAmount = youngAmount;
            MatureAmount = matureAmount;
            OldAmount = oldAmount;
            MatureReproduceCoefficient = matureReproduceCoefficient;
            OldReproduceCoefficient = oldReproduceCoefficient;
            YoungSurvivalCoefficient = youngSurvivalCoefficient;
            OldDyingCoefficient = oldDyingCoefficient;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    public class AnnualContract: ICloneable
    {
        public int FoodAmount { get; set; }
        public int FoodPrice { get; set; }
        public int FoodPenalty { get; set; }

        public int YoungAmount { get; set; }
        public int YoungPrice { get; set; }
        public int YoungPenalty { get; set; }

        public int MatureAmount { get; set; }
        public int MaturePrice { get; set; }
        public int MaturePenalty { get; set; }

        public int OldAmount { get; set; }
        public int OldPrice { get; set; }
        public int OldPenalty { get; set; }

        public AnnualContract() { }

        public AnnualContract(int foodAmount = 0, int foodPrice = 0, int foodPenalty = 0, int youngAmount = 0, int youngPrice = 0, int youngPenalty = 0,
            int matureAmount = 0, int maturePrice = 0, int maturePenalty = 0, int oldAmount = 0, int oldPrice = 0, int oldPenalty = 0)
        {
            FoodAmount = foodAmount;
            FoodPrice = foodPrice;
            FoodPenalty = foodPenalty;

            YoungAmount = youngAmount;
            YoungPrice = youngPrice;
            YoungPenalty = youngPenalty;

            MatureAmount = matureAmount;
            MaturePrice = maturePrice;
            MaturePenalty = maturePenalty;

            OldAmount = oldAmount;
            OldPrice = oldPrice;
            OldPenalty = oldPenalty;
        }

        public override string ToString()
        {
            return String.Format("Молодые: {0}/{1}/{2}, Взрослые: {3}/{4}/{5}, Старые: {6}/{7}/{8}, Корм: {9}/{10}/{11}", YoungAmount, YoungPrice, YoungPenalty, MatureAmount, MaturePrice, MaturePenalty, OldAmount, OldPrice, OldPenalty, FoodAmount, FoodPrice, FoodPenalty);
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}